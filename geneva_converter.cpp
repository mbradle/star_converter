////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to convert geneva star file.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#ifdef WN_USER
#include "master.h"
#else
#include "default/master.h"
#endif

#include <iostream>
#include <fstream>
#include <Libnucnet.h>

#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

//#############################################################################
// Defines.
//##############################################################################

#define I_QUANTITY  9
#define S_FILE      "input-star-file"

typedef std::map<std::string, size_t> index_map_t;

//##############################################################################
// getData().
//##############################################################################

class getData
{

  public:
    getData(){}
    getData( std::string& line )
    {
      typedef boost::tokenizer<boost::char_separator<char> > my_tok;
      boost::char_separator<char> sep( " " );

      my_tok tok( line, sep );

      std::vector<std::string> items;

      for( my_tok::iterator it = tok.begin(); it != tok.end();++it )
      {
        items.push_back( *it );
      }

      doReplacements( items );

      for( size_t i = 0; i < I_QUANTITY; i++ )
      {
        quantityMap[items[i]] = i + 1;
      }

      for( size_t i = I_QUANTITY; i < items.size(); i++ )
      {
        speciesMap[items[i]] = i + 1;
      } 

    }

    void
    doReplacements( std::vector<std::string>& items )
    {

      for( size_t i = 0; i < items.size(); i++ )
      {
        if( items[i] == "p" ) { items[i] = "h1"; }
        if( items[i] == "d" ) { items[i] = "h2"; }
        if( items[i] == "al*6" ) { items[i] = "al26"; }
      }

    }

    void
    doUpdates(
      std::vector<double>& items,
      nnt::Zone& zone
    )
    {
      index_map_t::iterator it;

      it = quantityMap.find( "temperature" );
      if( it != quantityMap.end() )
      {
        zone.updateProperty( nnt::s_T9, items[it->second] / 1.e9 );
      }

      it = quantityMap.find( "mass" );
      if( it != quantityMap.end() )
      {
        zone.updateProperty(
          "mass below", items[it->second] * GSL_CONST_CGSM_SOLAR_MASS
        );
      }

      it = quantityMap.find( "radius" );
      if( it != quantityMap.end() )
      {
        zone.updateProperty(
          "cell outer radius", items[it->second] 
        );
      }

      it = quantityMap.find( "temperature" );
      if( it != quantityMap.end() )
      {
        zone.updateProperty(
          "cell temperature", items[it->second]
        );
      }

      it = quantityMap.find( "density" );
      if( it!= quantityMap.end() )
      {
        zone.updateProperty( "cell density", items[it->second] );
        zone.updateProperty( nnt::s_RHO, items[it->second] );
      }
    }
      
    void
    operator()( std::string& line, Libnucnet * p_nucnet, nnt::Zone& zone )
    {
      typedef boost::tokenizer<boost::char_separator<char> > my_tok;
      boost::char_separator<char> sep( " " );

      my_tok tok( line, sep );

      std::vector<double> items;

      my_tok::iterator it = tok.begin();

      Libnucnet__relabelZone(
        p_nucnet,
        zone.getNucnetZone(),
        it->c_str(),
        "0",
        "0"
      ); 

      for( ; it != tok.end(); ++it )
      {
        items.push_back( boost::lexical_cast<double>( *it ) );
      }

      BOOST_FOREACH( index_map_t::value_type& v, quantityMap )
      {
        zone.updateProperty( v.first, items[v.second] );
      }

      BOOST_FOREACH( index_map_t::value_type& v, speciesMap )
      {
        Libnucnet__Species * p_species =
          Libnucnet__Nuc__getSpeciesByName(
            Libnucnet__Net__getNuc(
              Libnucnet__Zone__getNet( zone.getNucnetZone() )
            ),
            v.first.c_str()
          );
        if( p_species )
        {
          Libnucnet__Zone__updateSpeciesMassFraction(
            zone.getNucnetZone(),
            p_species,
            items[v.second]
          );
        }
      }

      doUpdates( items, zone );

    }

  private:
    index_map_t quantityMap;
    index_map_t speciesMap;

};

//##############################################################################
// main().
//##############################################################################

int main( int argc, char * argv[] )
{

  std::string line;

  wn_user::v_map_t v_map;

  //============================================================================
  // Get the input.
  //============================================================================

  wn_user::inputter my_input;

  my_input.addPositionalParameter<std::string>(
    S_FILE, 1, "input star text file (enter as option or positional parameter)",
    "in.txt"
  );

  v_map = my_input.getInput( argc, argv );

  //============================================================================
  // Set the network and output.
  //============================================================================

  wn_user::network_data my_network_data( v_map );

  my_network_data();

  wn_user::outputter my_outputter( v_map, my_network_data.getNucnet() );

  //============================================================================
  // Input.
  //============================================================================

  std::ifstream input_file;

  input_file.open(
    my_input.getPositionalParameter<std::string>( v_map, S_FILE ).c_str()
  );

  std::getline( input_file, line );

  getData my_data( line );

  //============================================================================
  // Loop on the data.
  //============================================================================

  while( std::getline( input_file, line ) )
  {

    nnt::Zone zone;

    Libnucnet__Zone * p_zone =
      Libnucnet__Zone__new(
        Libnucnet__getNet( my_network_data.getNucnet() ), "0", "0", "0"
      );
 
    zone.setNucnetZone( p_zone );

    my_data( line, my_network_data.getNucnet(), zone );
           
    my_outputter( zone );

  }

  input_file.close();

  //============================================================================
  // Output.
  //============================================================================

  my_outputter.write();

  return EXIT_SUCCESS;

}

